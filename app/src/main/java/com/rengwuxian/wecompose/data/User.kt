package com.rengwuxian.wecompose.data

import androidx.annotation.DrawableRes
import com.rengwuxian.wecompose.activity.R


data class User(
    val id: String,
    val name: String,
    @DrawableRes val avatar: Int
) {
    companion object {
        lateinit var Me: User
    }
}