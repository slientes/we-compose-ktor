package com.rengwuxian.wecompose.mv

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.jiayou.SimpleAuthPackPackageImpl
import com.jiayou.server.ClientSocketServer
import com.jiayou.session.DefaultClientSocketSession
import com.rengwuxian.wecompose.entity.User
import java.net.InetSocketAddress

/**
@author lishuang
@data   2021-11-13  17:06
@info   index page model view
 */

class IndexViewModel : ViewModel() {
    var title by mutableStateOf(false)
    var input by mutableStateOf(false)
    var login by mutableStateOf(false)
    var buttom by mutableStateOf(false)

    var username by mutableStateOf("")
    var password by mutableStateOf("")
    var logined by mutableStateOf(false)


    var friends: MutableList<User> by mutableStateOf(mutableListOf<User>())


    override fun toString(): String {
        return "IndexViewModel(title=$title, input=$input, login=$login, buttom=$buttom, username='$username', password='$password')"
    }

}