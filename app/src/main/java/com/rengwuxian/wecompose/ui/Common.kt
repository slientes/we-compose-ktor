package com.rengwuxian.wecompose.ui

import android.content.Context
import android.text.InputType
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cn.hutool.http.HttpRequest
import cn.hutool.json.JSONUtil
import com.qmuiteam.qmui.widget.dialog.QMUIDialog.EditTextDialogBuilder
import com.rengwuxian.wecompose.activity.MainActivity
import com.rengwuxian.wecompose.activity.R
import com.rengwuxian.wecompose.data.User
import com.rengwuxian.wecompose.mv.WeViewModel
import com.rengwuxian.wecompose.ui.theme.WeTheme
import dev.chrisbanes.accompanist.insets.navigationBarsPadding
import dev.chrisbanes.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.*
import kotlin.concurrent.thread

@Composable
fun WeTopBar(title: String, onBack: (() -> Unit)? = null) {
    Box(
        Modifier
            .background(WeTheme.colors.background)
            .fillMaxWidth()
            .statusBarsPadding()
    ) {
        Row(
            Modifier
                .height(48.dp)
        ) {
            if (onBack != null) {
                Icon(
                    painterResource(R.drawable.ic_back),
                    null,
                    Modifier
                        .clickable(onClick = onBack)
                        .align(Alignment.CenterVertically)
                        .size(36.dp)
                        .padding(8.dp),
                    tint = WeTheme.colors.icon
                )
            }
            Spacer(Modifier.weight(1f))
            val viewModel: WeViewModel = viewModel()
            Icon(
                painterResource(R.drawable.setting_active),
                "切换主题",
                Modifier
                    .clickable(onClick = {
                        showEditTextDialog(context = MainActivity.context)
                    })
                    .align(Alignment.CenterVertically)
                    .size(36.dp)
                    .padding(8.dp),
                tint = WeTheme.colors.icon
            )
            Icon(
                painterResource(R.drawable.ic_palette),
                "切换主题",
                Modifier
                    .clickable(onClick = {
                        viewModel.theme = when (viewModel.theme) {
                            WeTheme.Theme.Light -> WeTheme.Theme.Dark
                            WeTheme.Theme.Dark -> WeTheme.Theme.NewYear
                            WeTheme.Theme.NewYear -> WeTheme.Theme.Light
                        }
                    })
                    .align(Alignment.CenterVertically)
                    .size(36.dp)
                    .padding(8.dp),
                tint = WeTheme.colors.icon
            )
        }
        Text(title, Modifier.align(Alignment.Center), color = WeTheme.colors.textPrimary)
    }
}

@Composable
fun WeBottomBar(modifier: Modifier = Modifier, content: @Composable RowScope.() -> Unit) {
    Row(
        modifier
            .fillMaxWidth()
            .background(WeTheme.colors.bottomBar)
            .padding(4.dp, 0.dp)
            .navigationBarsPadding(),
        content = content
    )
}


fun showEditTextDialog(context: Context, finished: () -> Unit = {}) {
    val builder = EditTextDialogBuilder(context)
    builder.setTitle("添加好友")
        .setPlaceholder("请输入需要添加的好友ID")
        .setInputType(InputType.TYPE_CLASS_TEXT)
        .addAction(
            "取消"
        ) { dialog, index -> dialog.dismiss() }
        .addAction(
            "确定"
        ) { dialog, index ->
            val text: CharSequence? = builder.editText.text
            if (text != null && text.isNotEmpty()) {
                if (text.toString() == User.Me.id) {
                    GlobalScope.launch {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(
                                context,
                                "请不要添加自己，系统会bug",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    dialog.dismiss()
                    return@addAction
                }
                thread {
                    runBlocking {
                        val parse = JSONUtil.parseObj(
                            HttpRequest.post("http://${MainActivity.uri}/im/operation/getOperation")
                                .form(
                                    mapOf(
                                        "id1" to User.Me.id,
                                        "id2" to text.toString(),
                                        "operation" to 1
                                    )
                                )
                                .header("auth", "73913da7-508b-4646-b8ae-ef6458f41153")
                                .execute()
                                .body()
                        )
                        dialog.dismiss()
                        withContext(Dispatchers.Main) {
                            Toast.makeText(
                                context,
                                parse["data"].toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        finished.invoke()
                    }
                }
            } else {
                Toast.makeText(context, "请输入ID", Toast.LENGTH_SHORT).show()
            }
        }
        .show()
}