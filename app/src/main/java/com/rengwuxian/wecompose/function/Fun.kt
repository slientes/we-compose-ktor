package com.rengwuxian.wecompose.function

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager

/**
@author lishuang
@data   2021-11-13  15:56
@info
 */
fun fullScreen(activity: Activity, color: String? = null) {
    val window = activity.window
    val decorView: View = window.decorView
    //两个 flag 要结合使用，表示让应用的主体内容占用系统状态栏的空间
    val option = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
    decorView.systemUiVisibility = option
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    //设置状态栏为透明，否则在部分手机上会呈现系统默认的浅灰色
    if (color == null)
        window.statusBarColor = Color.TRANSPARENT
    else
        window.statusBarColor = Color.parseColor(color)
    activity.window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)

}