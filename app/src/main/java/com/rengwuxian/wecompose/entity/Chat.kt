package com.rengwuxian.wecompose.entity

import java.io.Serializable
import java.time.LocalDateTime

/**
@author 李 双
@data   2020-11-20  11:34
@info
 */

data class Chat(

    var id: Int? = null,
    /**
     * 用户1
     */
    var id1: String? = null,

    /**
     * 用户2
     */
    var id2: String? = null,

    /**
     * 消息
     */
    var text: String? = null,

    /**
     * 创建时间
     */
    var createTime: LocalDateTime? = null


) :  Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}
