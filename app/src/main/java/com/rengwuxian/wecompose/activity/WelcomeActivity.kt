package com.rengwuxian.wecompose.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import cn.hutool.core.util.RandomUtil
import kotlin.concurrent.thread

class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_hello)
        thread {
            Thread.sleep(RandomUtil.randomLong(100, 1000))
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}