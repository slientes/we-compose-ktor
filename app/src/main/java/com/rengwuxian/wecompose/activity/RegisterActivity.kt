package com.rengwuxian.wecompose.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import cn.hutool.core.util.ReUtil
import cn.hutool.http.HttpRequest
import cn.hutool.json.JSONUtil


import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.concurrent.thread

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private var type = 1     //1获取验证码     2//验证验证码
    private val properties = Properties()
    private lateinit var uri: String
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var email: EditText
    private lateinit var yanzhengma:EditText
    private lateinit var button: com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        properties.load(application.assets.open("im.properties"))
        uri = properties["im.uri"].toString() + ":" + properties["im.port"].toString()

        button = findViewById(R.id.im_login)
        username = findViewById(R.id.username)
        password = findViewById(R.id.password)
        email = findViewById(R.id.email)
        yanzhengma = findViewById(R.id.yanzhengma)
        button.setOnClickListener(this)
    }

    override fun onClick(p0: View) {
        when (p0.id) {
            R.id.im_login -> {
                zhuce()
            }
            else -> {
            }
        }
    }


    private fun zhuce() {

        if (username.text.length < 6 || password.text.length < 6) {
            Toast.makeText(this, "账户密码不得小于6位，", Toast.LENGTH_SHORT).show()
            return
        }

        if (!ReUtil.isMatch(
                "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*\$",
                email.text.toString()
            )
        ) {
            Toast.makeText(this, "请填写正确的邮箱！", Toast.LENGTH_SHORT).show()
            return
        }

        if (!ReUtil.isMatch("^\\w+\$", username.text.toString()) ||
            !ReUtil.isMatch("^\\w+\$", password.text.toString())
        ) {
            Toast.makeText(this, "账户密码不能出现非法字符", Toast.LENGTH_SHORT).show()
            return
        }



        if (type == 1)
            thread {
                runBlocking {

                    val parse = JSONUtil.parseObj(
                        HttpRequest.post("http://$uri/im/user/sendEmail")
                            .form(
                                mapOf(
                                    "email" to email.text.toString()
                                )
                            )
                            .header("auth", "73913da7-508b-4646-b8ae-ef6458f41153").execute()
                            .body()
                    )

                    if (parse["code"].toString().toInt() == 200) {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(
                                this@RegisterActivity,
                                parse["data"].toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    type = 2
                    button.text = "注册账户"
                }
            }
        else
            thread {
                runBlocking {
                    val parse = JSONUtil.parseObj(
                        HttpRequest.post("http://$uri/im/user/register")
                            .form(
                                mapOf(
                                    "username" to username.text.toString(),
                                    "password" to password.text.toString(),
                                    "email" to email.text.toString(),
                                    "code" to yanzhengma.text.toString()
                                )
                            )
                            .header("auth", "73913da7-508b-4646-b8ae-ef6458f41153").execute()
                            .body()
                    )
                    withContext(Dispatchers.Main) {
                        Toast.makeText(
                            this@RegisterActivity,
                            parse["data"].toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (parse["code"].toString().toInt() == 200)
                        finish()
                    button.text = "注册账户"
                }
            }

    }
}